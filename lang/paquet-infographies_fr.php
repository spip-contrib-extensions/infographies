<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'infographies_description' => 'Nouveaux objets pour réaliser des infographies',
	'infographies_nom' => 'Infographie',
	'infographies_slogan' => 'Nouveaux objets pour réaliser des infographies'
);

?>
