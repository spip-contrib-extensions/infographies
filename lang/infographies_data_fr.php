<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// B
	'bouton_ajouter_donnees' => 'Importer des données',
	'bouton_editer' => 'Éditer',
	'bouton_supprimer' => 'Supprimer',
	'bouton_supprimer_donnee_confirmation' => 'Êtes-vous sûr de vouloir supprimer la donnée ?',

	// E
	'erreur_aucune_donnee' => 'Aucune donnée n\'est associée à ce jeu de données.',
	'explication_url' => 'Adresse URL du jeu de données si il est externe.',

	// I
	'icone_creer_infographies_data' => 'Créer un jeu de données',
	'icone_modifier_infographies_data' => 'Modifier le jeu de données',
	'icone_supprimer_supprimer_infographies_data' => 'Supprimer le jeu de données',
	'info_1_infographies_data' => 'Un jeu de données',
	'info_aucun_infographies_data' => 'Aucun jeu de données',
	'info_axe_x' => 'Axe horizontal',
	'info_axe_y' => 'Axe vertical',
	'info_credits' => 'Crédits',
	'info_css_class' => 'Class CSS',
	'info_fichier' => 'Fichier de données',
	'info_nb_infographies_datas' => '@nb@ jeux de données',
	'info_nouveau_infographies_data' => 'Nouveau jeu de données',
	'info_retirer_infographies_data' => 'Retirer le jeu de données',
	'info_type' => 'Type des données',
	'info_type_externes' => 'externes',
	'info_type_internes' => 'internes',
	'info_unite' => 'Unité de mesure',
	'info_url' => 'URL des données',
	
	// L
	'lien_ajouter_infographies_data' => 'Ajouter ce jeu de données',
	'lien_tout_supprimer' => 'Supprimer toutes les données',
	
	// T
	'texte_ajouter_infographies_data' => 'Ajouter un jeu de données',
	'texte_creer_associer_infographies_data' => 'Créer et associer un nouveau jeu de données',
	'titre_donnees_liees' => 'Données liées',
	'titre_infographies_data' => 'Jeu de données',
	'titre_infographies_datas' => 'Jeux de données',
	'titre_logo_infographies_data' => 'Logo du jeu de données',
);

?>
